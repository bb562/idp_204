//Program to test simple forward motion of AGV
//IMPORTANT: Always calibrate straightspeed and turning times at the start of each session.
#include<Wire.h>
#include<Adafruit_MotorShield.h>
#include<Servo.h>

//Create motor shield object with default I2C address.
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *rmotor = AFMS.getMotor(1); //Initialising right side motor (assuming it is connected to port 1, change if necessary.)
Adafruit_DCMotor *lmotor = AFMS.getMotor(2); //Initialising left side motor (assuming it is connected to port 2, change if necessary.)
//Create servo objects
Servo ClawServo;
Servo TipperServo;

int rreflector = A0; //pin selection for reflection sensors
int mreflector = A1;
int lreflector = A2;
uint16_t rlight =0, llight = 0, mlight = 0; //variables to store relfection sensor reading
uint16_t lsensortrigger = 990, msensortrigger = 950, rsensortrigger = 750;

uint8_t straightspeed = 60, rspeed = 0, lspeed = 0; //Straight line speed of the AGV, changes reflect in all straight line motion.
uint16_t uturntime = 10000; //calibrate with changes to chassis
bool on = false; //logic marker for push switch
bool alldark = false, killed = false;
unsigned long t0 = millis(); //reference time for flashing LED
bool clawstate = false; //logic marker to track whether claw is open
int clawpos = 90, clawposmin = 45, clawposmax = 90; // variable to store the claw servo position
bool tipperstate = false; //logic marker to track whether the robot is tipped
int tipperpos = 150, tipperposmin = 20, tipperposmax = 150; // variable to store the tipper servo position
int mode = 0; //0 for line following, 1 for robot searching, 2 for robot retrieval
void chrspeed(uint8_t nspeed) //sets rmotor speed and updates rspeed
{
  if(rspeed == nspeed)
    return;
  else
    {
      if(nspeed != 0)
      {
      rmotor->setSpeed(nspeed);
      if(rspeed == 0)
      rmotor->run(BACKWARD);
     
      }
      else
      rmotor->run(RELEASE);
      rspeed = nspeed;
    }
}
void chlspeed(uint8_t nspeed) // sets lmotor speed and updates lspeed
{
  if(lspeed == nspeed)
    return;
  else
    {
      if(nspeed != 0)
      {
      
      lmotor->setSpeed(nspeed);
      if(lspeed == 0)
      lmotor->run(BACKWARD);
     
      }
      else
      lmotor->run(RELEASE);
      lspeed = nspeed;
    }
}
void reversel(uint8_t nspeed) // sets lmotor speed and updates lspeed
{
  if(lspeed == nspeed)
    return;
  else
    {
      if(nspeed != 0)
      {
      
      lmotor->setSpeed(nspeed);
      if(lspeed == 0)
      lmotor->run(FORWARD);
     
      }
      else
      lmotor->run(RELEASE);
      lspeed = nspeed;
    }
}
void reverser(uint8_t nspeed) //sets rmotor speed and updates rspeed
{
  if(rspeed == nspeed)
    return;
  else
    {
      if(nspeed != 0)
      {
      rmotor->setSpeed(nspeed);
      if(rspeed == 0)
      rmotor->run(FORWARD);
     
      }
      else
      rmotor->run(RELEASE);
      rspeed = nspeed;
    }
}
void steerleft() //function to steer left by adjusting motor speeds
{
  reversel(20);
  chrspeed(240);
}
void steerright() //function to steer right by adjusting motor speeds
{
  reverser(20);
  chlspeed(240);
}
void gostraight()
{
  chrspeed(straightspeed);
  chlspeed(straightspeed);
}
void turn_180() //function to turn 180 towards counter-clockwise.
{
  chrspeed(0);
  chlspeed(0);
  chrspeed(70);
  chlspeed(0);
  delay(uturntime); //Calibrate this value based on the time required for the robot to rotate 180deg.
  chrspeed(0);
 
}
void line_steering() //line following algorithm
{
  rlight = analogRead(rreflector);
  mlight = analogRead(mreflector);
  llight = analogRead(lreflector);
 
  if((mlight>=msensortrigger)&&(llight<lsensortrigger)&&(rlight<rsensortrigger))
  {
    gostraight();
  } 
  else if((llight>=lsensortrigger)&&(rlight<rsensortrigger))
  {
    steerleft();
  }
  else if((rlight>=rsensortrigger)&&(llight<lsensortrigger))
  {
    steerright();
  }
  else if((rlight>=rsensortrigger)&&(llight>=lsensortrigger)&&(mlight>=msensortrigger))
  {
    gostraight();
  }
  else if((rlight<rsensortrigger)&&(llight<lsensortrigger)&&(mlight<msensortrigger))
  {
    chrspeed(0);
    chlspeed(0);
    alldark = true;
  }
}
void toggleOnOff()
{
  on = !on;
  if(!on)
  {
    clawpos = clawposmax;
    ClawServo.write(clawpos);
    clawstate = false;
    tipperpos = tipperposmax;
    TipperServo.write(tipperpos);
    tipperstate = false;
    alldark = false;
    killed = false;
  }
  digitalWrite(7, LOW);
}

void flashLED()
{
  if((lspeed != 0)||(rspeed != 0))
  {
    if (millis() - t0 >= 250)
    {
      t0 = millis();
      if (digitalRead(7) == LOW)
      digitalWrite(7, HIGH);
      else
      digitalWrite(7, LOW);
    }
  }
  else
  digitalWrite(7, LOW);
}

void toggleClaw()
{
  if(clawstate)
  {
    //if claw closed, open
    for (clawpos = clawposmin; clawpos <= clawposmax; clawpos += 1)
    { 
      ClawServo.write(clawpos);
      delay(2);
    }
    clawstate = false;
  }
  else
  {
    //if claw open, close
    for (clawpos = clawposmax; clawpos >= clawposmin; clawpos -= 1)
    {
      ClawServo.write(clawpos);
      delay(2);
    }
    clawstate = true;
  }
}

void toggleTipper()
{
  if(tipperstate)
  {
    //if tipped, go level
    for (tipperpos = tipperposmin; tipperpos <= tipperposmax; tipperpos += 1)
    {
      TipperServo.write(tipperpos);
      delay(2);
    }
    tipperstate = false;
  }
  else
  {
    //if level, go tipped
    for (tipperpos = tipperposmax; tipperpos >= tipperposmin; tipperpos -= 1)
    {
      TipperServo.write(tipperpos);
      delay(2);
    }
    tipperstate = true;
  }
}

void setup()
{
  pinMode(0, OUTPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(5, INPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(11, INPUT);
  pinMode(12, INPUT);
  pinMode(13, OUTPUT);
  
  attachInterrupt(digitalPinToInterrupt(11), toggleOnOff, FALLING);

  AFMS.begin();

  //initialise servo positions
  ClawServo.write(clawpos);
  TipperServo.write(tipperpos);

  //attach servo objects
  ClawServo.attach(9);
  TipperServo.attach(10);
}

void loop() {
  while(on)
  {
    if(!alldark)
    {
      line_steering();
      flashLED();
    }
    else if(!killed)
    {
      gostraight();
      delay(10000);
      chrspeed(0);
      chlspeed(0);
      toggleTipper();
      delay(1000);
      toggleClaw();
      delay(1000);
      toggleTipper();
      delay(1000);
      toggleClaw();
      killed = true;
    }
  }
  chrspeed(0);
  chlspeed(0);
}